public with sharing class ExpenseController {
	
    @AuraEnabled
    public static List<Expense__c> getExpenses(){
        return [Select Id,Name,Amount__c ,Client__c, Date__c,Reimbursed__c 
                From Expense__c 
                Where CreatedById =: UserInfo.getUserId() ];
    }
    
    @AuraEnabled
    public static Expense__c createExpense(Expense__c expense){
        System.debug('THE EXPENSE ===>'+expense);
        Database.upsert(expense);
        return expense;
    }
}