({
	doInit  : function(component, event, helper) {
		var myDate = component.get("v.expense.Date__c");
        if(myDate){
            component.set("v.formatdate",new Date(myDate));
        }
	},
    clickReimbursed:function(component,event,helper){
        var thisExpense = component.get("v.expense");
        console.log('checked expense==>',thisExpense);
    }
})