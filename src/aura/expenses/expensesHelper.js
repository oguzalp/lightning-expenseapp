({
    createExpense: function(component, expense) {
        var theExpenses = component.get("v.expenses");
        // Copy the expense to a new object
        var newExpense = Object.assign({},expense); //JSON.parse(JSON.stringify(expense));
 		var action  = component.get("c.createExpense");
        action.setParams({"expense" : newExpense});
        action.setCallback(this,function(result){
            var state = result.getState();
            if(state === "SUCCESS"){
                 theExpenses.push(result.getReturnValue());
        		 component.set("v.expenses", theExpenses);
            }else{
                console.error('error create expense==>', result.getError());
            }
        });
        $A.enqueueAction(action);
       
    },
    getExpenses:function(component){
        var action = component.get("c.getExpenses");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
               	var expenses = response.getReturnValue(); 
                component.set("v.expenses",expenses);
            }else if(state === "ERROR"){
                console.error('An error occured!===>'+response.getError()[0]);
            }
        });
        $A.enqueueAction(action);
    }
})