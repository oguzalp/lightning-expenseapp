## Useful links for Salesforce Developers ##

* [Create your own developer organization](https://developer.salesforce.com/signup)
* [Apex Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_dev_guide.htm)
* [Visualforce Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_intro.htm)
* [Lightning Components Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/intro_framework.htm)
* [Lightning Design System](https://www.lightningdesignsystem.com)
* [Open Source Aura FrameWork](http://documentation.auraframework.org/auradocs)
* [Start Learning with Trailhead](https://trailhead.salesforce.com/en/modules)


## You can contact me .. ##

* Email : oguz.alp@osf-global.com
* [My LinkedIn Profile](https://www.linkedin.com/in/oguzalp/) 